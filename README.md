# matrix_determinant_konopada
This repository has been created as part of semestral work in 
B6B36PJC C++ course.

## Determinant of square matrix computation
This gitlab project implements determinant of a square 
matrix computation using LU decomposition. 

Determinant is computed 
by simply multiplying numbers 
of diagonals. That is thanks to the fact that if A = LU and
det(A) = det(L)det(U) and determinants of L and U are simply
products of the elements on diagonal. This attitude speeds the 
process of determinant computation significantly opposed to 
computation from determinant definition.

## Usage
This repository serves as a Clion project with Cmake and can 
be compiled using CMAkeList.

### Matrix generator
You can use your own text file or use matrix_to_file generator.
The project contains a generator of file containing matrix of a given size. 
It can be run as:

WINDOWS:
```
.\matrix_to_file.exe <FILE_NAME.txt> <MATRIX_SIZE>
```
LINUX:
```
./matrix_to_file <FILE_NAME.txt> <MATRIX_SIZE>
```

### Determinant computation
To compute determinant of generated matrix from file, run:

WINDOWS:
```
.\Semestralka.exe <FILE_NAME.txt>
```
LINUX:
```
./Semestralka <FILE_NAME.txt>
```

## Time
Thanks to the LU decomposition, the computation of a determinant of 
1000x1000 matrix took **8.009 miliseconds.**


