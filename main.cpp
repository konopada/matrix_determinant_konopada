#include <iostream>
#include <fstream>
#include "Matrix.h"
#include <chrono>
#include <bits/stdc++.h>

template<typename T>
void print_matrix(const std::vector<std::vector<T>> &mat){
    for (size_t i = 0; i < mat.size(); i++){
        for (size_t j = 0; j < mat.size(); j++){
            std::cout << mat[i][j] << " ";
        }
        std::cout << std::endl;
    }
}

void help(){
    std::cout << "Hello, this is help of a file that computes matrix determinant for you." << std::endl;
    std::cout << "Please as a first and only argument insert the name of the .txt with your matrix." << std::endl;
    std::cout << "Just wait for the computed determinant on the standard output." << std::endl;
}

int main(int argc, char* argv[]) {

    if(argc != 2){
        std::cout << "Please insert the correct number of arguments." << std::endl;
        std::cout << "Run file with --help if you are unsure." << std::endl;
        return 0;
    }

    if(strcmp(argv[1], "--help") == 0){
        help();
        return 0;
    }

    std::ifstream m_file;
    m_file.open(argv[1]);
    if(!m_file)
    {
        std::cout << "Wrong argument or non-existing or corrupted input file." << std::endl;
        return -1;
    }

    int temp;
    int numberOfInputs = 0;
    // get size of given matrix
    while(!m_file.eof())
    {
        m_file >> temp;
        ++numberOfInputs;
    }
    int size = std::sqrt(numberOfInputs-1);
    std::cout << "Size of given matrix is: " << size << std::endl;
    m_file.close();

    // Generate instance of a Matrix class
    m_file.open(argv[1]);
    Matrix mat(size);
    mat.parse_m_file(m_file);

    // Decompose, compute determinant and measure time
    auto start = std::chrono::high_resolution_clock::now();
    mat.LU_decompose();
    mat.compute_determinant();
    auto end = std::chrono::high_resolution_clock::now();

    // Final results output
    std::cout << "Determinant computation took: " <<
    std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << " miliseconds" <<  std::endl;
    std::cout << "Determinant is: "<< mat.determinant << std::endl;

    m_file.close();
    return 0;
}
