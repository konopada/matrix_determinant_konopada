//
// Created by akono on 28.12.2021.
//

#include "Matrix.h"
#include <thread>
#include <mutex>
#include <omp.h>
#include <bits/stdc++.h>

Matrix::Matrix(int size) {
    determinant = 1;
    mat_size = size;
    matrix = std::vector<std::vector<int>> (size, std::vector<int>(size));
    L = std::vector<std::vector<double>> (size, std::vector<double>(size));
    U = std::vector<std::vector<double>> (size, std::vector<double>(size));
    for (int i = 0; i < mat_size; ++i) {
        for (int j = 0; j < mat_size; ++j) {
            matrix[i][j] = 0;
            U[i][j] = 0;
            L[i][j] = 0;
        }
    }
}

void Matrix::parse_m_file(std::ifstream &m_file) {
    for (int i = 0; i < mat_size; ++i) {
        for (int j = 0; j < mat_size; ++j) {
            m_file >> matrix[i][j];
        }
    }
}

void Matrix::LU_decompose() {

    for (int i = 0; i < matrix.size(); ++i) {
        for (int j = 0; j < matrix.size(); ++j) {
            if(j<i){L[j][i] = 0;} else{
                L[j][i] = matrix[j][i];
                for (int k = 0; k < i; ++k) {
                    L[j][i] = L[j][i] - L[j][k] * U[k][i];
                }
            }
        }
        for (int j = 0; j < matrix.size(); ++j) {
            if(j<i){U[i][j] = 0;} else if (j==i){
                U[i][j] = 1;
            } else{
                U[i][j] = matrix[i][j] / L[i][i];
                for (int k = 0; k < i; ++k) {
                    U[i][j] = U[i][j] - ((L[i][k] * U[k][j]) / L[i][i]);
                }
            }
        }
    }
}

void Matrix::compute_determinant() {
    determinant = 1;
    for (int i = 0; i < matrix.size(); ++i) {
        determinant = determinant * L[i][i] * U[i][i];
    }
}