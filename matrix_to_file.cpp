//
// Created by akono on 28.12.2021.
//
#include <iostream>
#include <vector>
#include <bits/stdc++.h>

void help(){
    std::cout << "Hello, this is help of matrix to file generator." << std::endl;
    std::cout << "Please as a first argument insert the name of the .txt file you want to be generated." << std::endl;
    std::cout << "Second argument is the size of the square matrix to be generated." << std::endl;
}

int main(int argc, char* argv[]) {

    if(argc != 3){
        std::cout << "Please insert the correct number of arguments." << std::endl;
        std::cout << "Run file with --help if you are unsure." << std::endl;
        return 0;
    }

    if(strcmp(argv[1], "--help") == 0){
        help();
        return 0;
    }

    std::cout << "Generating matrix of size: " << atoi(argv[2]) << std::endl;
    int size = atoi(argv[2]);

    std::ofstream m_file;
    m_file.open(argv[1]);
    // generate square matrix of given size and fill it with random numbers between 1 and 100
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            m_file << rand() % 100 + 1 << " ";
        }
        m_file << std::endl;
    }
    std::cout << "Matrix has been generated to the file: " << argv[1] << std::endl;

    m_file.close();

    return 0;
}
